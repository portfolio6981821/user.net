using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.IdentityModel.Tokens;

public partial class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();

        // Add services to the container.
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddCors(CreateCors);

       // builder.Services.AddScoped<IUserRepository, UserRepository>(); // While building for all constructors
                                                                       // with IUserRepository give them UserRepository 
        AddAuthentication(builder);
        WebApplication app = BuildApp(builder);

        app.MapControllers();
        app.Run();
    }

    private static void CreateCors(CorsOptions options)
    {
        options.AddPolicy("DevelopmentCors", (corsBuilder) =>
        {
            corsBuilder.WithOrigins("http://localhost:4200", "http://localhost:3000", "http://localhost:8000")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials();
        });

        options.AddPolicy("ProductionCors", (corsBuilder) =>
        {
        corsBuilder.WithOrigins("https://myProductionSite.com") // FrontEnd Domain
        .AllowAnyMethod()
        .AllowAnyHeader()
        .AllowCredentials();
        });
    }

    private static void AddAuthentication(WebApplicationBuilder builder)
    {
        string tokenKey = "";
        string? nullableTokenKey = builder.Configuration.GetSection("Appsettings:TokenKey").Value;
        if (nullableTokenKey != null)
        {
            tokenKey = nullableTokenKey;
        }

        SymmetricSecurityKey securityKey = new SymmetricSecurityKey(
         Encoding.UTF8.GetBytes(tokenKey));

        TokenValidationParameters tokenValidationParameters = new TokenValidationParameters()
        {
            IssuerSigningKey = securityKey,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = false,
            ValidateAudience = false
        };

        builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddJwtBearer(Options =>
        {
            Options.TokenValidationParameters = tokenValidationParameters;
        });
    }

    private static WebApplication BuildApp(WebApplicationBuilder builder)
    {
        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseCors("DevelopmentCors");
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        else
        {
            app.UseCors("ProductionCors");
            app.UseHttpsRedirection();
        }

        app.UseAuthentication();
        app.UseAuthorization();
        return app;
    }
}