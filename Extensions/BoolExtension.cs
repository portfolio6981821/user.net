public static class BoolExtension
{
    public static string ConvertToBit(this bool value)
    {
        if (value)
        {
            return "1";
        }

        return "0";
    }
}