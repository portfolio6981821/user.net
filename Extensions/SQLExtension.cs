﻿using Dapper;
using System.Data;
using System.Text;

public static class SQLExtension
{
    public static (string, DynamicParameters) ConvertNullableParameteres(Dictionary<string, Object?> objectsTotest)
    {
        StringBuilder sb = new StringBuilder();
        DynamicParameters dynamicParameters = new DynamicParameters();
        foreach (var item in objectsTotest)
        {
            if (item.Value != null)
            {
                string parameterName = item.Key + "Parameter";

                if (!string.IsNullOrEmpty(sb.ToString()))
                {
                    sb.Append(",");
                }
                sb.Append(" ");
                sb.Append(item.Key);
                sb.Append("=");
                sb.Append(parameterName);

                dynamicParameters.Add(parameterName, item.Value, item.Value.SQLParameterType());
            }
        }

        return (sb.ToString(), dynamicParameters);
    }

    public static DbType SQLParameterType(this Object type)
    {
        if (type is string text)
        {
            return DbType.String;
        }
        else if (type is DateTime dateTime)
        {
            return DbType.Time;
        }
        else if (type is decimal number)
        {
            return DbType.Decimal;
        }
        else if (type is int integer)
        {
            return DbType.Int32;
        }
        else if (type is bool boolean)
        {
            return DbType.Boolean;
        }

        return DbType.Object;
    }

    public static string? ToSqlString(this Object? obj)
    {
        if (obj == null)
        {
            return "";
        }
        else if (obj is string text)
        {
            return StringOvveride(text);
        }
        else if (obj is DateTime dateTime)
        {
            return dateTime.FormatDateMMDDRRRR();
        }
        else if (obj is decimal number)
        {
            return number.ConvertForSql();
        }

        return obj.ToString();
    }

    private static string StringOvveride(string? obj)
    {
        if (obj != null)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("'");
            sb.Append(obj.EscapeSingleQuote());
            sb.Append("'");

            return sb.ToString();
        }

        return "";
    }
}