using System.Text;

public static class StringExtension
{
    public static string EscapeSingleQuote(this string input)
    {
        string output = input.Replace("'", "''");
        return output;
    }
}