public static class DecimalExtension
{
    public static string ConvertForSql(this decimal input)
    {
        return input.ToString().Replace(",", ".");
    }
}