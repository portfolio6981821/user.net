public static class DateExtension
{
    public static string FormatDateMMDDRRRR(this DateTime dateToFormat)
    {
        string? date = dateToFormat.ToString();
        if (date != null)
        {
            string day = date[0].ToString() + date[1].ToString();
            string month = date[3].ToString() + date[4].ToString();

            string newDate = String.Format("{0}.{1}{2}", month, day, date.Substring(5));
            return newDate;
        }

        return "";
    }
}