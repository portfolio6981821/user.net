using Dapper;
using Microsoft.Data.SqlClient;

namespace DotnetAPI.Data
{
    public class DataContextDapper
    {
        private readonly IConfiguration _configuration;

        private string? _connectionString
        {
            get => _configuration.GetConnectionString("DefaultConnection");
        }

        public DataContextDapper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IEnumerable<T> LoadData<T>(string sqlCommand)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            return databaseConnection.Query<T>(sqlCommand);
        }

        public T LoadDataSingle<T>(string sqlCommand)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);

            T? data = databaseConnection.QuerySingle<T>(sqlCommand);
            if (data == null)
            {
                throw new Exception($"failed to load object");
            }

            return data;
        }

        public T? LoadNullableSingleData<T>(string sqlCommand) where T : class
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            try
            {
                return databaseConnection.QuerySingle<T>(sqlCommand);
            }
            catch
            {
                return null;
            }
        }

        public bool ExecuteSql(string sqlCommand)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            return databaseConnection.Execute(sqlCommand) > 0;
        }

        public int ExecuteSqlWithRowCount(string sqlCommand)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            return databaseConnection.Execute(sqlCommand);
        }

        public bool ExecuteSqlWithParameters(string sql, DynamicParameters parameters)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            return databaseConnection.Execute(sql, parameters) > 0;
        }

        public virtual IEnumerable<T> LoadDataWithParameters<T>(string sql, DynamicParameters parameters)
        {
            SqlConnection databaseConnection = new SqlConnection(_connectionString);
            return databaseConnection.Query<T>(sql, parameters);
        }
    }
}