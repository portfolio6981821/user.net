namespace DotnetAPI.Models.DTO
{
    public partial class UserForLoginDTO
    {
        public string Email { get; set; } = "";
        public string Password { get; set; } = "";
    }
}