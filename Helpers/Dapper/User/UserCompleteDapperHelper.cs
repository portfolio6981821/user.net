﻿using Dapper;
using DotnetAPI.Data;
using DotnetAPI.Models;

public class UserCompleteDapperHelper
{
    private readonly DataContextDapper _dapper;
    public UserCompleteDapperHelper(DataContextDapper dapper)
    {
        _dapper = dapper;
    }

    public bool UpsertUser(UserComplete user)
    {
        string sqlComand = String.Format(@"
                  EXEC [UserData].[spUser_Upsert]");
        Dictionary<string, object?> nullableParameters = new Dictionary<string, object?>()
                {
                    {"@Salary", user.Salary},
                    {"@FirstName", user.FirstName},
                    {"@LastName", user.LastName},
                    {"@Email", user.Email},
                    {"@Gender", user.Gender},
                    {"@Active", user.Active},
                    {"@UserId", user.UserId},
                    {"@JobTitle", user.JobTitle},
                    {"@Department", user.Department}
                };

        (string parameters, DynamicParameters dynamicParameters) = SQLExtension.ConvertNullableParameteres(nullableParameters);
        sqlComand += parameters;
        return _dapper.ExecuteSqlWithParameters(sqlComand, dynamicParameters);
    }
}
