using AutoMapper;
using DotnetAPI.Data;
using DotnetAPI.Models;
using DotnetAPI.Models.DTO;

namespace DotnetAPI.Controllers.Register
{
    public class RegistrationHandler : IRegistrator
    {
        private readonly DataContextDapper _dapper;
        private readonly Mapper _mapper;
        private readonly IPasswordCreator _passwordCreator;
        private readonly UserCompleteDapperHelper _userHelper;

        public RegistrationHandler(DataContextDapper dapper, IPasswordCreator passwordCreator)
        {
            _dapper = dapper;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserForRegistrationDTO, UserComplete>()
                    .ForMember(dest => dest.Active, opt => opt.MapFrom(src => true));
                cfg.CreateMap<UserForRegistrationDTO, UserForLoginDTO>();
            });
            _mapper = new Mapper(config);
            _passwordCreator = passwordCreator;
            _userHelper = new UserCompleteDapperHelper(_dapper);
        }

        public bool DoesEmailExist(string email)
        {
            string sqlCommand = String.Format(
                "SELECT * FROM UserData.Users WHERE Email = '{0}'",
                email.EscapeSingleQuote());

            IEnumerable<UserForLoginConfirmationDTO> users = _dapper
            .LoadData<UserForLoginConfirmationDTO>(sqlCommand);

            bool exist = users.Any(user => user.Email == email);
            return exist;
        }

        public bool InsertNewUser(UserForRegistrationDTO registrationDTO)
        {
            var user = _mapper.Map<UserForLoginDTO>(registrationDTO);
            if (_passwordCreator.UpsertPassword(user))
            {
                return AddUserInformation(registrationDTO);
            }

            return false;
        }

        private bool AddUserInformation(UserForRegistrationDTO userForRegistration)
        {
            UserComplete user = _mapper.Map<UserComplete>(userForRegistration);
            return _userHelper.UpsertUser(user);
        }
    }

    public interface IRegistrator
    {
        public bool DoesEmailExist(string Email);
        public bool InsertNewUser(UserForRegistrationDTO registrationDTO);
    }
}