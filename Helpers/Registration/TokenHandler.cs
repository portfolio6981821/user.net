using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using DotnetAPI.Data;
using Microsoft.IdentityModel.Tokens;

namespace DotnetAPI.Controllers.Register
{
    public class UserToken : IToken
    {
        private readonly IConfiguration _configuration;
        private readonly DataContextDapper _dapper;

        public UserToken(IConfiguration configuration, DataContextDapper dapper)
        {
            _configuration = configuration;
            _dapper = dapper;
        }

        public Dictionary<string, string> GetTokenDictionary(string email)
        {
            return GetTokenDictionary(GetUserIdByEmail(email));
        }

        public Dictionary<string, string> GetTokenDictionary(int userId)
        {
            return new Dictionary<string, string>
            {
                {"token", CreateToken(userId)}
            };
        }

        public string CreateToken(int userID)
        {
            SecurityTokenDescriptor descriptor = GetDescriptor(userID);

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(descriptor);

            return tokenHandler.WriteToken(token);
        }

        private SecurityTokenDescriptor GetDescriptor(int userID)
        {
            Claim[] claims = GetClaim(userID);
            string tokenKey = GetTokenKey();

            SymmetricSecurityKey securityKey = new SymmetricSecurityKey(
             Encoding.UTF8.GetBytes(tokenKey));

            SigningCredentials credentials = new SigningCredentials(
               securityKey, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor descriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(claims),
                SigningCredentials = credentials,
                Expires = DateTime.Now.AddDays(1)
            };
            return descriptor;
        }

        private string GetTokenKey()
        {
            string tokenKey = "";
            string? nullableTokenKey = _configuration.GetSection("Appsettings:TokenKey").Value;
            if (nullableTokenKey != null)
            {
                tokenKey = nullableTokenKey;
            }

            return tokenKey;
        }

        private Claim[] GetClaim(int userID)
        {
            Claim[] claims = new Claim[]
            {
                new Claim("userID", userID.ToString())
            };

            return claims;
        }

        private int GetUserIdByEmail(string email)
        {
            string sqlCommand = String.Format(@"
            SELECT UserId FROM UserData.Users WHERE Email = '{0}'", email);
            int userId = _dapper.LoadDataSingle<int>(sqlCommand);

            return userId;
        }
    }

    public interface IToken
    {
        public string CreateToken(int id);
        public Dictionary<string, string> GetTokenDictionary(string identificator);
        public Dictionary<string, string> GetTokenDictionary(int userId);
    }
}