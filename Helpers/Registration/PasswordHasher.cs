using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace DotnetAPI.Controllers.Register
{
    public class PasswordHasher : IHasher
    {
        private readonly IConfiguration _configuration;

        public PasswordHasher(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public byte[] GetPasswordSalt()
        {
            byte[] passwordSalt = new byte[128 / 8];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetNonZeroBytes(passwordSalt);
            }

            return passwordSalt;
        }

        public byte[] GetPasswordHash(string password, byte[] passwordSalt)
        {
            string? passwordKey = _configuration.GetSection("AppSettings:PasswordKey").Value;
            string passwordSaltPlusString = passwordKey + Convert.ToBase64String(passwordSalt);

            return KeyDerivation.Pbkdf2(
                password,
                salt: Encoding.ASCII.GetBytes(passwordSaltPlusString),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8
            );
        }
    }

    public interface IHasher
    {
        public byte[] GetPasswordHash(string password, byte[] passwordSalt);
        public byte[] GetPasswordSalt();
    }
}