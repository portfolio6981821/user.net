using DotnetAPI.Data;
using DotnetAPI.Models.DTO;

namespace DotnetAPI.Controllers.Register
{
    public class LoginHandler : ILogger
    {
        private readonly DataContextDapper _dapper;
        private readonly IHasher _hasher;

        public LoginHandler(DataContextDapper dapper, IHasher hasher)
        {
            _dapper = dapper;
            _hasher = hasher;
        }

        public bool CheckPassword(UserForLoginDTO userForLoginDTO, UserForLoginConfirmationDTO confirmation)
        {
            byte[] passwordHash = _hasher.
            GetPasswordHash(userForLoginDTO.Password, confirmation.PasswordSalt);

            bool doesHashSame = DoesHashMatch(passwordHash, confirmation.PasswordHash);
            return doesHashSame;
        }

        private bool DoesHashMatch(byte[] hash1, byte[] hash2)
        {
            for (int i = 0; i < hash1.Length; i++)
            {
                if (hash1[i] != hash2[i])
                {
                    return false;
                }
            }
            return true;
        }

        public UserForLoginConfirmationDTO GetConfirmationData(UserForLoginDTO loginDTO)
        {
            string sqlForConformation = String.Format(@"SELECT [Email],
                    [PasswordHash],
                    [PasswordSalt] 
                    FROM UserData.Auth WHERE Email = '{0}'",
            loginDTO.Email.EscapeSingleQuote());

            var data = _dapper.LoadNullableSingleData<UserForLoginConfirmationDTO>(sqlForConformation);

            if (data != null)
            {
                return data;
            }

            throw new Exception("Cant find matching email");
        }
    }

    public interface ILogger
    {
        public UserForLoginConfirmationDTO GetConfirmationData(UserForLoginDTO loginDTO);
        public bool CheckPassword(UserForLoginDTO userForLoginDTO, UserForLoginConfirmationDTO confirmation);
    }
}