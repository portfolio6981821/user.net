﻿using Dapper;
using DotnetAPI.Data;
using DotnetAPI.Models.DTO;
using Microsoft.Data.SqlClient;
using System.Data;

namespace DotnetAPI.Controllers.Register
{
    public class PasswordCreator : IPasswordCreator
    {
        private readonly DataContextDapper _dapper;
        private readonly IHasher _passwordHasher;

        public PasswordCreator(DataContextDapper dapper, IHasher hasher)
        {
            _dapper = dapper;
            _passwordHasher = hasher;
        }

        public bool UpsertPassword(UserForLoginDTO user)
        {
            UserForLoginConfirmationDTO confirmation = GetLoginConfirmationDTO(user);
            return UpsertAuthentication(confirmation);
        }

        private bool UpsertAuthentication(UserForLoginConfirmationDTO userForLogin)
        {
            string sqlCommand = @"EXEC [UserData].[spRegistration_Upsert] 
                                        @PasswordHash=@PasswordHashParam,
                                        @PasswordSalt=@PasswordSaltParam,
                                        @Email=@EmailParam";

            DynamicParameters sqlParameters = GetParameters(userForLogin);
            bool succesfullyAddPassword = _dapper.ExecuteSqlWithParameters(sqlCommand, sqlParameters);
            return succesfullyAddPassword;
        }

        private DynamicParameters GetParameters(UserForLoginConfirmationDTO confirmation)
        {
            DynamicParameters sqlParameters = new DynamicParameters();
            sqlParameters.Add("@PasswordSaltParam", confirmation.PasswordSalt, DbType.Binary);
            sqlParameters.Add("@PasswordHashParam", confirmation.PasswordHash, DbType.Binary);
            sqlParameters.Add("@EmailParam", confirmation.Email, DbType.String);

            return sqlParameters;
        }

        private UserForLoginConfirmationDTO GetLoginConfirmationDTO(UserForLoginDTO user)
        {
            byte[] passwordSalt = _passwordHasher.GetPasswordSalt();
            byte[] passwordHash = _passwordHasher.GetPasswordHash(user.Password, passwordSalt);

            UserForLoginConfirmationDTO confirmationDTO = new UserForLoginConfirmationDTO
            {
                PasswordHash = passwordHash,
                PasswordSalt = passwordSalt,
                Email = user.Email
            };

            return confirmationDTO;
        }
    }

    public interface IPasswordCreator
    {
        bool UpsertPassword(UserForLoginDTO password);
    }
}