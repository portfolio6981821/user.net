using DotnetAPI.Data;
using DotnetAPI.Models.DTO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotnetAPI.Controllers.Register
{
    [Authorize]
    [ApiController]
    [Route("Controller")]
    public class AuthenthicationController : ControllerBase
    {
        private readonly IToken _tokenCreator;
        private readonly DataContextDapper _dapper;
        private readonly IConfiguration _configuration;
        private readonly IRegistrator _registrationHandler;
        private readonly ILogger _loginHandler;
        private readonly IPasswordCreator _passwordCreator;

        public AuthenthicationController(IConfiguration configuration)
        {
            _configuration = configuration;
            _dapper = new DataContextDapper(_configuration);

            IHasher hasher = new PasswordHasher(_configuration);
            _passwordCreator = new PasswordCreator(_dapper, hasher);
            _registrationHandler = new RegistrationHandler(_dapper, _passwordCreator);
            _loginHandler = new LoginHandler(_dapper, hasher);
            _tokenCreator = new UserToken(_configuration, _dapper);
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public IActionResult Register(UserForRegistrationDTO userForRegistarionDTO)
        {
            if (!userForRegistarionDTO.DoesPasswordsMatch())
            {
                throw new Exception("Passwords do not match!");
            }

            if (_registrationHandler.DoesEmailExist(userForRegistarionDTO.Email))
            {
                throw new Exception("User already exist");
            }

            if (!_registrationHandler.InsertNewUser(userForRegistarionDTO))
            {
                throw new Exception("Failed to add user");
            }

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public IActionResult Login(UserForLoginDTO userForLoginDTO)
        {
            var confirmation = _loginHandler.GetConfirmationData(userForLoginDTO);

            if (_loginHandler.CheckPassword(userForLoginDTO, confirmation))
            {
                return Ok(_tokenCreator.GetTokenDictionary(userForLoginDTO.Email));
            }

            return StatusCode(401, "Incorrect Password");
        }

        [HttpPut("ResetPassword")]
        public IActionResult ResetPassword(UserForLoginDTO userForSetPassword)
        {
            if (_loginHandler.GetConfirmationData(userForSetPassword) == null)
            {
                throw new Exception("cant find matching email");
            }

            if (_passwordCreator.UpsertPassword(userForSetPassword))
            {
                return Ok();
            }

            throw new Exception("Reset password failed");
        }

        [HttpGet("RefreshToken")]
        public IActionResult RefreshToken()
        {
            string userID = User.FindFirst("userId")?.Value + "";

            string userIdSql = "SELECT userId FROM UserData.Users Where UserId = " + userID;

            int userIdFromDB = _dapper.LoadDataSingle<int>(userIdSql);
            return Ok(_tokenCreator.GetTokenDictionary(userIdFromDB));
        }
    }
}