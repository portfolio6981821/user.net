using Dapper;
using DotnetAPI.Data;
using DotnetAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotnetAPI.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class PostController : ControllerBase
    {
        private readonly DataContextDapper _dapper;
        public PostController(IConfiguration configuration)
        {
            _dapper = new DataContextDapper(configuration);
        }

        [HttpGet("GetPosts")]
        public IEnumerable<Post> GetPosts(int? postId = null, int? userId = null, string? searchParameter = null)
        {
            string sqlCommand = @"EXEC UserData.spPosts_Get";
            Dictionary<string, Object?> nullableParams = new Dictionary<string, object?>
            {
                {"@PostId", postId},
                {"@UserId", userId },
                {"@SearchValue", searchParameter }
            };
            (string parameters, DynamicParameters dynamicParameters) = SQLExtension.ConvertNullableParameteres(nullableParams);
            return _dapper.LoadDataWithParameters<Post>(sqlCommand + parameters, dynamicParameters);
        }

        [HttpGet("MyPosts")]
        public IEnumerable<Post> GetMyPosts()
        {
            string sqlCommand = @"
            EXEC UserData.spPosts_Get 
            @UserId=" +
            this.User.FindFirst("userId")?.Value;

            return _dapper.LoadData<Post>(sqlCommand);
        }

        [HttpPut("UpsertPost")]
        public IActionResult UpsertPost(Post postToAdd)
        {
            string sqlCommand = String.Format(@"
                EXEC [UserData].[spPosts_Upsert]");
            Dictionary<string, Object?> nullableParameters = new Dictionary<string, object?>()
            {
                {"@UserId", this.User.FindFirst("userId")?.Value },
                {"@PostTitle", postToAdd.PostTitle},
                {"@PostContent", postToAdd.PostContent},
                {"@PostId", postToAdd.PostId > 0 ? postToAdd.PostId : null}
            };

            (string parameters, DynamicParameters dynamicParameters) = SQLExtension.ConvertNullableParameteres(nullableParameters);
            if (_dapper.ExecuteSqlWithParameters(sqlCommand, dynamicParameters))
            {
                return Ok();
            }

            throw new Exception("Failed to upsert post");
        }

        [HttpGet("PostsBySearch/{searchParameter}")]
        public IEnumerable<Post> PostsBySearch(string searchParameter)
        {
            string sqlCommand = String.Format(@"
            SELECT * FROM UserData.Posts
            Where PostTitle LIKE '%{0}%'
            OR PostContent LIKE '%{0}%'",
            searchParameter.EscapeSingleQuote());

            return _dapper.LoadData<Post>(sqlCommand);
        }

        [HttpDelete("DeletePost/{ID}")]
        public IActionResult DeleteOne(int ID)
        {
            string sqlCommand = String.Format(@"UserData.spPost_Delete
                 @UserId={0}, 
                 @PostId={1}",
                User.FindFirst("userId")?.Value, ID.ToString());

            if (_dapper.ExecuteSql(sqlCommand))
            {
                return Ok();
            }

            throw new Exception("Failed to Delete post");
        }
    }
}