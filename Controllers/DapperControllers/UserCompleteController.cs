using Dapper;
using DotnetAPI.Data;
using DotnetAPI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DotnetAPI.Controllers.DapperControllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UserCompleteController : ControllerBase
    {
        private readonly DataContextDapper _dapper;
        private readonly UserCompleteDapperHelper _userHelper;
        public UserCompleteController(IConfiguration configuration)
        {
            _dapper = new DataContextDapper(configuration);
            _userHelper = new UserCompleteDapperHelper(_dapper);
        }

        [HttpGet("GetSingleCompleteUser/{userID}")]
        public UserComplete GetSingleUser(int userID)
        {
            string sqlCommand = $"EXEC UserData.spUsers_Get @UserId={userID}";
            return _dapper.LoadDataSingle<UserComplete>(sqlCommand);
        }

        [HttpGet("GetCompleteUsers")]
        public IEnumerable<UserComplete> GetUsers(int? userId, bool? isActive)
        {
            string sqlCommand = @"EXEC UserData.spUsers_Get";
            Dictionary<string, object?> nullableParams = new Dictionary<string, object?>
            {
                { "@UserId", userId },
                { "@Active", isActive }
            };
            (string parameters, DynamicParameters dynamicParameters) = SQLExtension.ConvertNullableParameteres(nullableParams);

            return _dapper.LoadDataWithParameters<UserComplete>(sqlCommand + parameters, dynamicParameters);
        }

        [HttpPut("UpsertUser")]
        public IActionResult UpsertUser(UserComplete user)
        {
            if (_userHelper.UpsertUser(user))
            {
                return Ok();
            }

            throw new Exception("Failed to update CompleteUser");
        }

        [HttpDelete("DeleteOne/{userID}")]
        public IActionResult DeleteOne(int userID)
        {
            string sqlComand = $"EXECUTE @RC = [UserData].[spUser_Delete] @UserId{userID}";

            if (_dapper.ExecuteSql(sqlComand))
            {
                return Ok();
            }

            throw new Exception("Failed to delete user complete");
        }
    }
}